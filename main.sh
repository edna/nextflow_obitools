## argument data path
DATA_FOLDER=$1
REFERENCE_DATABASE=$2
REFERENCE_DATABASE_FASTA=$3

## main pipeline
## demultiplexing and filtering of metabarcoding raw data
./nextflow run scripts/step1.nf  --datafolder ${DATA_FOLDER}
## concatenate cleaned sample sequences by run id
bash scripts/step2.sh
## taxonomic assignment and generating matrix species/sample for each run
./nextflow run scripts/step3.nf  --db_ref ${REFERENCE_DATABASE} --db_fasta ${REFERENCE_DATABASE_FASTA}
## done check work/ and runs/ folders for intermediate files and tables/ folder for final results
