## collect list of run id
for f in `ls work/*/*/*.u.f.p.cleaned.fa`;
do basename $f | cut -d "." -f 1 ;done |sort | uniq > run.list
## concatenate sample files from the same run id
mkdir runs
cat run.list | while read RUN ;
do
 cat work/*/*/${RUN}.u.f.p.cleaned.fa > runs/${RUN}.fasta
done
rm run.list