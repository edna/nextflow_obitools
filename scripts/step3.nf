params.db_ref="/media/superdisk/edna/donnees/basedereference/embl_std"
params.db_fasta="/media/superdisk/edna/donnees/basedereference/db_embl_std.fasta"

fastaruns=Channel.fromPath("runs/*.fasta").map { file -> tuple(file.baseName, file) }


process dereplicate_runs {
  """
  Dereplicate and merge samples together
  """
  publishDir 'runs'
  input:
    set RUN_ID, file(fastarun) from fastaruns
  output:
    set RUN_ID, file("${RUN_ID}.uniq.fa") into fastaRunUniqs
  script:
  """
  obiuniq -m sample ${fastarun} > ${RUN_ID}.uniq.fa
  """
}

process assign_taxon {
  """
  Assign each sequence to a taxon
  """
  publishDir 'runs'
  input:
    set RUN_ID, file("${RUN_ID}.uniq.fa") from fastaRunUniqs
  output:
    set RUN_ID, file("${RUN_ID}.u.tag.fa") into assigneds
  script:
  """
  ecotag -d ${params.db_ref} -R ${params.db_fasta} ${RUN_ID}.uniq.fa > ${RUN_ID}.u.tag.fa
  """
}

process rm_attributes {
  """
  Some unuseful attributes can be removed at this stage
  """
  publishDir 'runs'
  input:
    set RUN_ID, file("${RUN_ID}.u.tag.fa") from assigneds
  output:
    set RUN_ID, file("${RUN_ID}.u.t.lessattributes.fa") into lessattributes
  script:
  """
  obiannotate --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
 --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
 --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
 --delete-tag=obiclean_head  --delete-tag=obiclean_headcount \
 --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
 --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
 --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
 --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
 --delete-tag=tail_quality ${RUN_ID}.u.tag.fa > ${RUN_ID}.u.t.lessattributes.fa
  """
}

process sort_runs {
  """
  The sequences can be sorted by decreasing order of count
  """
  publishDir 'runs'
  input:
    set RUN_ID, file("${RUN_ID}.u.t.lessattributes.fa") from lessattributes
  output:
    set RUN_ID, file("${RUN_ID}.u.t.l.sorted.fa") into sorteds
  script:
  """
  obisort -k count -r ${RUN_ID}.u.t.lessattributes.fa > ${RUN_ID}.u.t.l.sorted.fa
  """
}

process table_runs {
  """
  Generate a table final results
  """
  publishDir 'tables'
  input:
    set RUN_ID, file("${RUN_ID}.u.t.l.sorted.fa") from sorteds
  output:
    set RUN_ID, file("${RUN_ID}.csv") into runtables
  script:
  """
  obitab -o ${RUN_ID}.u.t.l.sorted.fa > ${RUN_ID}.csv
  """
}
